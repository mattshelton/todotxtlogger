=begin
Plugin: TodoTxt Logger
Description: Imports the day's completed todo.txt items into DayOne
Author: [Matt Shelton](http://mattshelton.net)
Configuration:
  'todo_file_path': ''
  'done_file_path': ''
  'tags': '#todo'
Notes:
  - Specify the path to your todo.txt and done.txt files
=end

config = {
  'description' => ['This will take the latest completed todo.txt items and slog them',
                    'Specify the paths to your todo.txt and done.txt files'],
  'todo_file_path' => '',
  'done_file_path' => '',
  'tags' => '#todo'
}
$slog.register_plugin({ 'class' => 'TodoTxtLogger', 'config' => config })

class TodoTxtLogger < Slogger
  def do_log
    if @config.key?(self.class.name)
      config = @config[self.class.name]
      # check for a required key to determine whether setup has been completed or not
      if !config.key?('todo_file_path') || config['todo_file_path'] == []
        @log.warn("TodoTxt has not been configured or an option is invalid, please edit your slogger_config file.")
        return
      else
        # set any local variables as needed
        todo_name = config['todo_file_path']
        done_name = config['done_file_path']
        @tags = config['tags'] || ''
      end
    else
      @log.warn("TodoTxt has not been configured or an option is invalid, please edit your slogger_config file.")
      return
    end
    @log.info("Logging Completed Todo.txt Items")

    # Get today's date in format YYYY-MM-DD (%F)
    d = DateTime.now()
    @today = d.strftime("%F")

    # Populate the completed, formatted items into done_content via parse_completed(input_file)
    done_content = parse_completed(todo_name)
    done_content << parse_completed(done_name)

    # We only want an entry if there are completed items
    if ! done_content.empty?

      # Populate tags
      the_tags = "#{@tags}\n" unless @tags == ''

      # Populate options
      options = {}
      options['datestamp'] = Time.now.utc.iso8601
      options['starred'] = false
      options['uuid'] = %x{uuidgen}.gsub(/-/,'').strip
      options['content'] = "## Todo.txt Completed Items\n\n#{done_content}\n#{the_tags}"

      # Create a journal entry
      sl = DayOne.new
      sl.to_dayone(options)
    end

  end

  def parse_completed(input_file)
    # Parse out the lines which contain 'x yyyy-mm-dd'
    # Strip off 'x yyyy-mm-dd' and replace it with '*'
    # return as markdown-formatted list item with '\n' after

    completed = String.new

    file_read = File.readlines(input_file)
    file_read.each do |file_item|
      file_item.strip()
      if file_item[0] == 'x'
        if file_item =~ /#{@today}/
          cpt_item = file_item.gsub( /x #{@today}/, "*")  # Search and replace "yyyy-mm-dd" for "*""
          completed << cpt_item  # shove cpt_item and a carriage return onto the end of completed
        end
      end
     end

   return completed
  end
end
