# TodoTxt logger

A simple [Slogger](https://github.com/ttscoff/Slogger) plugin to journal your day's completed [todo.txt](http://todotxt.com) items in [Day One](http://dayoneapp.com). The plugin takes three parameters:

1. The full path to your todo.txt file. (Required)
2. The full path to your done.txt file. (Required)
3. Any tags you want added to your post. 

*This plugin was partially inspired by [todo2day1](https://github.com/sjsrey/todo2day1) because it no longer works and wasn't a plugin, so here we are.*
